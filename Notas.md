## La base de datos es nuestro archivo db.json. 
Hemos instalado el server-json, podemos acceder a la base de datos mediante 

json-server --watch db.json

Los cambios que hagamos en la app, cambiarán el archivo db.json y viceversa.

    ....    .....       ......

## Commit 16 (d09cf368): Los useKeydown composable (cómo funcionan)

Llamamos a useKeydown que coge un argumento, una array de objetos, cada uno conteniendo una key y una función. 

Arriba tenemos el método onKeydown  que ha sido registrado como un evenlistener (window.addEventListener etc.)
Luego al cerrarse el modal, quitamos el event listener (?)  así que sólo tenemos uno siendo registrado. 
Esto es muy útil si queremos registrar muchos keyboard events. 

Luego buscamos a través de la array de keyCombos y buscamos el que coincida con la key que hemos presionado. Si encuentra alguno, llama a la función. 

Ahora lo interesante será convertir todo el código : 

  ```
    let useKeydown = (keyCombos) => {
  let onKeydown = (event) => {
    console.log(event.key)
    let kc = keyCombos.find(kc =>kc.key == event.key)
    if (kc) {
      kc.fn();
    }
  };
  window.addEventListener("keydown", onKeydown);

  onBeforeUnmount(() => {
    window.removeEventListener("keydown", onKeydown);
  })
} 
```

En un composable. Lo cortaremos de ModalView.vue, incluyendo el import onBeforeUnmount.

E importaremos useKeydown de un archivo que crearemos luego. 

Creamos la carpeta composables y el archivo (que será .js y no .vue). 
Copiamos lo que habíamos cortado y lo pegamos (podemos quitar el console.log que no sirve ya para nada.)
Y eso sería, se podría quitar lo que se hace con el enter, que es también una chorradilla. 

De este modo, si queremos hacer lo mismo con otros componentes, sólo serán un par de líneas de código. 


...   ...     ...             ....      ...     ....    ...   ..    


## Commit 24 (f0e2dfd5): App-wide-nonpersisted state 

Tenemos un set reactivo que hace que al clickar el checkbox de un email, se actualice el número de emails seleccionados. 
Sin embargo sólo puede actuar en el componente MailTable, si queremos acceder a dicha información, la tenemos que poner en un composable. 

Usaremos la composition API para manipular  la app-wide non persisted state.

Cortamos el fragmento de código que teníamos 
```
let selected = reactive (new Set())

    let emailSelection = {
      emails:selected,
      toggle(email){
        if(selected.has(email)){
          selected.delete(email)
        }else{
          selected.add(email)
        }
      }
    }

    const state = reactive({
      openedEmail: null,
    });

    ```

    Y en el setup convertimos el valor de  emailSelection en una función useEmailSelection.  Importamos la función useEmailSelection, que haremos a continuación. 

  En el fichero nuevo use-email-slection.js, adecuamos el código que habíamos cortado y lo exportamos

  ```
  import { reactive } from 'vue'
export const useEmailSelection = function () {
    let emails = reactive(new Set())
    let toggle = function (email) {
        if (emails.has(email)) {
            emails.delete(email)
        } else {
            emails.add(email)
        }
    }
    return {
        emails,
        toggle
    }
}

export default useEmailSelection
```

Puesto que hemos usado el export, en MailTable, emailSelection recibirá toggle e emails. Así que seguirá funcionando (en los divs aparecía emailSelection.toggle etc. )

Sin embargo sigue estando todo dentro de MailTable. 
Para hacer la prueba nos llevamos el 
<h1>{{ emailSelection.emails.size }} emails selected</h1>
que cuenta cuántos emails han sido seleccionados a APP.vue. 

Importamos el composable y lo declaramos igual en setup (emailSlection: useEmailSelection())

Efectiamente, hay dos contadores de emails seleccionados, pero sólo una se actualiza. 

Por qué? Difícil de entender se supone que crea dos Set distintos. 

pero al sacar la línea 

```let emails = reactive(new Set())```
fuera de la función , ya va bien. POrque se usa el mismo set cada vez que se usa useEmailSelection. 


... ... ...               ....        ....      ... 


## Commit 26 (c51295ad) Quitando código duplicado de las funciones para los botones del bulk bar. 

. Codigo inicial:

```
   let markRead = () => {
        emails.forEach((email) => {
            email.read = true
            axios.put(`http://localhost:3000/emails/${email.id}`, email)
        })
    }

    let markUnread = () => {
        emails.forEach((email) => {
            email.read = false
            axios.put(`http://localhost:3000/emails/${email.id}`, email)
        })
    }

    let archive = ()=>{
        emails.forEach((email)=>{
            email.archived= true
      axios.put(`http://localhost:3000/emails/${email.id}`, email);
           
        })
    }```

    Las tres funciones hacen una iteración por cada email, luego hacen algo y luego lo suben con axios. ES la segunda parte la que es distinta. 

    Código final: 

    ```
  let forSelected = (fn) => {
        emails.forEach((email) => {
            fn(email)
            axios.put(`http://localhost:3000/emails/${email.id}`, email)
        })
    }

    let markRead = () => forSelected(e => e.read = true)
    let markUnread = () => forSelected(e => e.read = false)
    let archive = () => forSelected(e => e.archived = true)

    ```

De categoria, xe! 





