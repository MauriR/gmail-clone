import axios from 'axios'
import { reactive, ref } from 'vue'
let emails = reactive(new Set())

export const useEmailSelection = function () {
    let toggle = function (email) {
        if (emails.has(email)) {
            emails.delete(email)
        } else {
            emails.add(email)
        }
    }
    let clear = () => {
        emails.clear()//usa el clear propio del objeto Set. 
    }

    let addMultiple = (newEmails) => {
        newEmails.forEach((email) => {
            emails.add(email)
        })
    }
    let forSelected = (fn) => {
        emails.forEach((email) => {
            fn(email)
            axios.put(`http://localhost:3000/emails/${email.id}`, email)
        })
    }

    let markRead = () => forSelected(e => e.read = true)
    let markUnread = () => forSelected(e => e.read = false)
    let archive = () => {forSelected(e => e.archived = true); clear()} //clear porque si no, se quedaba marcado como seleccionado a pesar de haber sido archivado. 



    return {
        emails,
        toggle,
        clear,
        addMultiple,
        markRead,
        markUnread,
        archive
    }
}

export default useEmailSelection